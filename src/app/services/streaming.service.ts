import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { RadioService } from './radio.service';
import { RdsService } from './rds.service';

@Injectable({
  providedIn: 'root'
})
export class StreamingService {
  playing:boolean = false;
  player = false;
  audio:HTMLAudioElement = new Audio();
  rds : Observable<any>;
  channel:any;
  public status: EventEmitter<boolean> = new EventEmitter<boolean>();
  events = [
    'ended', 'error', 'play', 'playing', 'pause', 'canplay', 'loadedmetadata', 'loadstart', 'waiting', 'volumechange','suspend','seeked'
  ];

  constructor(
    public radioService: RadioService,
    public rdsService : RdsService
  ) {
    for (let index = 0; index < this.events.length; index++) {
      const value = this.events[index];
      this.audio.addEventListener(value, () => {
        console.log(value);
        this.eventsPlayer(value);
      });
    }
  }

  eventsPlayer(event){
    switch (event) {
      case 'ended':
        this.playing = false;
        this.status.emit(this.playing);        
        this.pause();
        this.presentAlert();
      break;
      case 'error':
        this.playing = false;
        this.status.emit(this.playing);        
        this.pause();
        this.presentAlert();
      break;
      case 'play':
        break;
      case 'playing':
        this.audio.muted = false;
        this.playing = true;
        this.status.emit(this.playing);
        // this.musicControls.updateIsPlaying(this.playing);
        // this.showControls();
        let channel = this.getChannels();
        this.loadRds(channel[0].rds);
      break;
      case 'pause':
        this.playing = false;
        this.status.emit(this.playing);
      break;
      case 'canplay':
      break;
      case 'loadedmetadata':
      break;
      case 'loadstart':
      break;
      case 'wating':
        this.playing = false;
        this.status.emit(this.playing);
      break;
      default:
        break;
    }
  }

  getChannels = () => {
    let channels = this.radioService.radio.channels;
    console.log('-->channels<--',channels);
    return channels;
  }
  

  loadStreaming = (channels) => {
    this.pause();
    localStorage.setItem('channels', JSON.stringify(channels));
    let lenghtChannel =  Object.keys(channels);
    if(lenghtChannel.length > 0){
      for(let i = 0; i < lenghtChannel.length; i++){
        let channel = channels[i];
        if(channel.main){
          this.channel = channel;
          this.load(channel);
        }
      }
    }else{
      this.presentAlert('404');
    }
  }
  async presentAlert(codigo:string = '') {
    let cod = codigo ? codigo : this.audio.error.code;
  }
  verifyStreaming(){
    
    this.audio.onerror = () => {
      console.log('-->error<--', this.audio.error);
      
      return false;
    }
    return true;
  }
  
  changeVolume(volume){
    console.log(volume);
    this.audio.volume = volume;
  }
  play(){
    if(this.player){
          this.audio.play()
            .catch(_=>{})
    }else{
      this.loadStreaming(this.getChannels());
    }
  }
  togglePlay = () =>{
    console.log('-->togglePlay<--', this.playing);
    if(this.playing){
      this.pause();
    }else{
      this.loadStreaming(this.getChannels());
    }
  }
  pause(){
    this.audio.pause();
  }
  load(channel){
    this.pause();
    let links:any = '';
    if(channel.highStreams[0]){
      links = channel.highStreams[0];
    }
    console.log('-->channel<--', channel);
    
    console.log('-->load streamings<--', links);
    this.push(links);
    return true;
  }
  push(links:string=''){
    console.log('-->links<--', links)
    if(links){
      this.audio.src = links;
      this.player = true;
      this.play();
    }else{
      this.presentAlert('404');
    }
  }
  loadRds(linkRds){
    let layout = '1';
    console.log('-->radio rds<--', linkRds);
    if(linkRds && this.playing){
      this.rdsService.loadRds(linkRds, layout)
      .then(result=>{
        console.log('-->RDS retorno<--', result);
        this.rds = this.rdsService.convertToRds(result, layout);
        this.rdsService.setRds(this.rds);
        setTimeout(
          _=>this.loadRds(linkRds),
          60000)
        });
    }
  }
  destroy(){
  }
}
