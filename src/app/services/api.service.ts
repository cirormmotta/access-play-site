import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl = 'https://ehtqi0vu11.execute-api.us-east-1.amazonaws.com/prod';
  apiSecretKey = 'B3dE5UTohE862OhBxjEPB35iositNImO13QbJ6h1';
  version:any = '1.0.0';
  urlConfig = '/assets/config/conf.json?date='+Math.random();
  httpOptions = {
    headers : {},
    params: {}
  }; 
  constructor(
    private httpClient: HttpClient
  ) {
    this.httpOptions = {
      headers : new HttpHeaders({
         'X-Api-Key' : this.apiSecretKey, 
         'Content-Type':'application/json',
         'Accept' : 'application/json'
       }),
       params: {}
     };
  }

  getConfs() {
    return this.httpClient.get('assets/conf.json');
  };

  getConfigsApp = (version) => {
    this.version = version;
    return this.request('/configs/'+this.version);
  };

  getRadio = (radioId) => this.request('/' + radioId, {});
  
  postSendEmail = (mail) => this.post('/sendemail/', mail).toPromise();

  postListeners = (user) => this.post('/listeners/', user).toPromise();
  putListeners = (user) => this.put('/listeners/', user).toPromise();
  getListeners = (id) => this.get('/listeners/' + id).toPromise();
  putListenersApp = (radioId, user) => this.put('/'+radioId+'/listeners/', user).toPromise();
  
  getPosts = (radioId, limit, offset, categorie) => this.get('/' + radioId + '/blog/post-list/' + limit + '/' + offset+'/'+categorie).toPromise();
  getPost = (radioId, url) => this.get('/' + radioId + '/blog/post/' + url).toPromise();
  getCategories = (radioId) => this.get('/' + radioId + '/blog/categories').toPromise();

  request (uri, params = {}, method = 'GET') : Observable<any> {
    
    this.httpOptions.params = params;
    return this.httpClient
    .request<any>(
      method,
      this.apiUrl+uri,
      this.httpOptions
    );
  }
  post (uri, params = {}) : Observable<any> {
    return this.httpClient
    .post<any>(
      this.apiUrl+uri,
      params,
      this.httpOptions
    );
  }
  put (uri, params = {}) : Observable<any> {
    return this.httpClient
    .put<any>(
      this.apiUrl+uri,
      params,
      this.httpOptions
    );
  }
  get (uri, params = {}) : Observable<any> {
    
    this.httpOptions.params = params;
    return this.httpClient
    .request<any>(
      'GET',
      this.apiUrl+uri,
      this.httpOptions
    );
  }
}
