import { Injectable, EventEmitter } from '@angular/core';
import { Rds, RdsXml } from '../shared/rds';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxXml2jsonService } from 'ngx-xml2json';
@Injectable({
  providedIn: 'root'
})
export class RdsService {
  public rds:Rds;
  public getRds:any = new EventEmitter();  
  constructor(
    private httpClient: HttpClient,
    private ngxXml2jsonService : NgxXml2jsonService
  ) { }

  loadRds(link='', type='json'):Promise<any>{
    let promise : Promise<any>;
    let configs:any = {
      responseType: 'json'
    }
    if(type!=='1'){
      configs.responseType = 'text'
    }
    if(link){
      promise = this.httpClient
      .get(
        link,
        configs
      )
      .toPromise()
    }
    return promise
  }
  convertToRds(rds, layout = '1'){
    let rdsClass:any;
    if(rds){
      if(layout == '1'){
        rdsClass = {
          artist: rds.musica_play.interprete,
          songTitle: rds.musica_play.titulo
        }
      }else if(layout == '2'){
        const obj:any = this.convertXmlToObj(rds);
        rdsClass = {
          artist: obj.musicas.musica.interprete,
          songTitle: obj.musicas.musica.titulo
        }
      }else if(layout == '3'){
        const obj:any = this.convertXmlToObj(rds);
        rdsClass = {
          artist: obj.Playing.item.Artist,
          songTitle: obj.Playing.item.SongTitle
        }
      }else if(layout == '4'){
        rds = rds.replace('AEV', '');
        rds = rds.replace('RDS', '');
        const obj:any = rds.split(";");
        rdsClass = {
          artist: obj[0],
          songTitle: obj[1]
        }
      }else if(layout == '5'){
        const obj:any = rds.split("-");
        rdsClass = {
          artist: obj[0],
          songTitle: obj[1]
        }
      }else if(layout == '6'){
        rds = rds.replace('PS_TEXT=', '');
        console.log(rds);
        const obj:any = rds.split("-");
        rdsClass = {
          artist: obj[0],
          songTitle: obj[1]
        }
      }
    }
    return rdsClass;
  }
  setRds(rds){
    this.getRds.emit(rds);
  }
  convertXmlToObj(rds){
    const parser = new DOMParser();
    const xml = parser.parseFromString(rds, 'text/xml');
    return this.ngxXml2jsonService.xmlToJson(xml);
  }
}
