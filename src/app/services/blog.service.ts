import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { RadioService } from './radio.service';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  posts:any;
  radio:any;
  categories: any;
  constructor(
    private apiService:ApiService,
    private radioService:RadioService
  ) {
    this.radio = this.radioService.radio;
    this.radioService.getRadio
      .subscribe(radio=>{
        this.radio = radio;
      });
  }
  getPosts(qtd = 8, offset=0, categorie = ''){
    return this.apiService.getPosts(this.radio.radioId, qtd, offset, categorie);
  }
  getPost(url){
    return this.apiService.getPost(this.radio.radioId, url);
  }
  getCategories(){
    return this.apiService.getCategories(this.radio.radioId);
  }
  setPosts(posts){
    this.posts = posts;
  }
}
