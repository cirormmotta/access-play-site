import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RadioService {

  constructor() { }

  radio:any;
  configs:any;
  getRadio: EventEmitter<any> = new EventEmitter<any>();
  setRadio(radio){
    this.radio = radio;
    this.getRadio.emit(radio);
  }
  setConfigs(configs){
    this.configs = configs;
  }
  getConfigs(){
    return this.configs;
  }
  getLogo = () =>{
    if(!this.radio.logo){
      return '';
    }
    return this.getUrlFiles()+'app/'+ this.radio.logo_site;
  }
  getUrlFiles = () =>{
    return this.getConfigs().link_file + this.radio.radioId + '/';
  }
  getSocial:any = () =>{
    if(this.radio.social){
      let social:any = {};
      for (let index = 0; index < this.radio.social.length; index++) {
        if(this.radio.social[index].type == 'whatsapp'){
          social[this.radio.social[index].type] = this.radio.social[index].value.replace(/\D/g, '');
        }else{
          social[this.radio.social[index].type] = this.radio.social[index].value;
        }
      }
      return social;
    }
    return;
  }
  theme:any = {
    bg: "#263238",
    player: "#0094a7",
    icons: "#263238",
    fonts: "#fff",
    menu: "#4f5b62",
    degrade: "linear-gradient(#6ef8ff, #0296a9)",
  }

  getTheme = () => {

    return this.radio.color ? this.radio.color : this.theme;
  }

  loadColors = (theme:any = {}) =>{
    this.theme = theme ? theme : this.getTheme();
    console.log(Object.keys(this.theme));
    let keys = Object.keys(this.theme);
    for (let index = 0; index < keys.length; index++) {
      console.log(keys[index], this.theme[keys[index]]);
      document.documentElement.style.setProperty('--color-'+keys[index], this.theme[keys[index]]);
    }
  }
}
