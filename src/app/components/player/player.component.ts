import { Component, OnInit } from '@angular/core';
import { RadioService } from '../../services/radio.service';
import { StreamingService } from '../../services/streaming.service';
import { RdsService } from '../../services/rds.service';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  radio:any;
  playing:boolean = false;
  rds:any;
  showShare:boolean = false;
  constructor(
    private radioService:RadioService,
    private streamingService:StreamingService,
    private rdsService:RdsService
  ) {
    this.streamingService.status
      .subscribe(
        playing=>{
          this.playing = playing;
        }
      );
    this.rdsService.getRds
      .subscribe(
        rds=>{
          console.log('getRds', rds)
          this.rds = rds;
        }
      );
  }

  toggleMenu(){
    
  }
  ngOnInit() {
    this.radio = this.radioService.radio;
    this.loadPlayer();
    this.radioService.getRadio
      .subscribe(radio=>{
        this.radio = radio;
        this.loadPlayer();
      });
  }
  loadPlayer(){
    console.log('-->load channels<--');
    if(this.radio){
      // this.streamingService.play();
      console.log('-->volume<--', this.streamingService.audio.volume);
    }
  }

  toggleShare($event){
    console.log($event);
    this.showShare = $event;
  }
  share(){
    this.showShare = true;
  }
  togglePlay(){
    this.streamingService.togglePlay();
  }
  changeVolume(event){
    this.streamingService.changeVolume((event.value/100) * 1);
  }
  value: number = 100;
  options: Options = {
    floor: 0,
    ceil: 100,
    showSelectionBar: true,
    selectionBarGradient: {
      from: '#fff',
      to: '#67f3fb'
    },
    getPointerColor: (value: number): string => {
      return '#2dccde';
    },
    hidePointerLabels: true,
    hideLimitLabels: true
  }

}
