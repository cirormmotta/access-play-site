import { Component, OnInit } from '@angular/core';
import { RadioService } from '../../services/radio.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  radio:any;
  social:any;
  logo:any;
  openMenu:boolean = false;
  constructor(
    private radioService:RadioService,
  ) { }

  toggleMenu(){
    this.openMenu = !this.openMenu;
  }
  ngOnInit() {
    this.radioService.getRadio
      .subscribe(radio=>{
        console.log(radio);
        this.radio = radio;
        this.social = this.radioService.getSocial();
        this.logo = this.radioService.getLogo();
        console.log(this.social);
      });
  }

}
