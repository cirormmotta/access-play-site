import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareComponent implements OnInit {
  @Input() show: boolean = false;
  @Output() share = new EventEmitter<boolean>();
  sociais:any = [
    {
      name: "facebook",
      color: "#4267B2"
    },
    {
      name: "twitter",
      color: "#00acee"
    },
    {
      name: "linkedin",
      color: "#006fa6"
    },
    {
      name: "whatsapp",
      color: "#25D366"
    },
    {
      name: "telegram",
      color: "#0088cc"
    },
    {
      name: "pinterest",
      color: "#BD091D"
    }
  ]
  constructor() { }

  ngOnInit() {
  }
  close(){
    this.share.emit(false);
  }
}
