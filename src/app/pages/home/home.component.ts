import { Component, OnInit } from '@angular/core';
import { RadioService } from '../../services/radio.service';
import { BlogService } from '../../services/blog.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  imageUrlArray:any;
  radio:any;
  logo:any;
  posts:any;
  social:any;
  constructor(
    private radioService : RadioService,
    private blogService : BlogService
  ) {
    this.radio = this.radioService.radio;
    this.posts = this.blogService.posts;
    if(this.radio){
      this.loadBanner(this.radio.banners);
    }
    this.loadPosts();
    this.loadContact();
    this.radioService.getRadio
      .subscribe(radio=>{
        console.log(radio);
        this.radio = radio;
        this.loadBanner(this.radio.banners);
        this.loadPosts();
        this.loadContact();
      });
  }
  loadContact(){
    if(this.radio){
      this.logo = this.radioService.getLogo();
      this.social = this.radioService.getSocial();
    }
  }
  loadBanner(bannersRadio){
    if(bannersRadio){
      console.log(bannersRadio);
      let banners = [];
      let configs = this.radioService.configs;
      for (let index = 0; index < bannersRadio.length; index++) {
        banners.push(configs.link_file + bannersRadio[index].file);
      }
      this.imageUrlArray = banners;
    }
  }
  loadPosts(){
    if(this.radio){
      this.blogService.getPosts()
        .then(posts=>{
          this.posts = posts;
          this.blogService.setPosts(posts);
          console.log(posts);
        })
    }
  }

  ngOnInit() {
  }
}
