import { Component } from '@angular/core';
import { ApiService } from './services/api.service';
import { RadioService } from './services/radio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'access-play-site';
  constructor(
    private apiService : ApiService,
    private radioService : RadioService
  ){

  }
  openMenu = false;
  toggleMenu() {
    if(this.openMenu){
      this.openMenu = false;
    }else{
      this.openMenu = true;
    }
  }
  ngOnInit(){
    var id = document.getElementById('loader');
    this.apiService.getConfs().toPromise()
      .then(confs=>{
        let config:any = confs;
        this.apiService.getConfigsApp(config.version).toPromise()
          .then(conf=>{
            this.radioService.setConfigs(conf);
            this.apiService.getRadio(config.radioId).toPromise()
            .then(radio =>{
              id.remove();
              this.radioService.setRadio(radio);
              this.radioService.loadColors(radio.color);
              var link = document.createElement('link');
              var title = document.createElement('title');
              title.innerHTML = radio.name;
              link.type = 'image/x-icon';
              link.rel = 'shortcut icon';
              link.href = this.radioService.getLogo();
              document.getElementsByTagName('head')[0].appendChild(link)
                                                      .appendChild(title);
            });
          })
      });
  }
}