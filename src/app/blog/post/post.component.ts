import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RadioService } from '../../services/radio.service';
import { BlogService } from '../../services/blog.service';
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  imageUrlArray:any;
  radio:any;
  post:any;
  url:string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private blogService:BlogService,
    private radioService:RadioService
  ) {
    this.radio = this.radioService.radio;
    this.loadPost();
    this.radioService.getRadio
      .subscribe(radio=>{
        console.log(radio);
        this.radio = radio;
        this.loadPost();
      });
  }
  loadPost(){
    if(this.radio){
      this.activatedRoute.params.subscribe(params=>{
        console.log(params);
        this.url = params.url;
        if(this.url){
          this.blogService.getPost(this.url)
          .then(post=>{
            console.log(post);
            this.post = post;
          });
        }
      });
    }
  }

  ngOnInit() {
  }

}
