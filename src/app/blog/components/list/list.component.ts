import { Component, OnInit, Input } from '@angular/core';
import { RadioService } from '../../../services/radio.service';
@Component({
  selector: 'app-blog-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() type: string;
  @Input() posts: any;
  configs:any;
  radio:any;
  constructor(
    private radioService: RadioService
  ) {
    this.radio = this.radioService.radio;
    this.loadPosts();
    this.radioService.getRadio
      .subscribe(radio=>{
        this.radio = radio;
        this.loadPosts();
      })
  }
  loadPosts(){
    if(this.radio){
      this.radio = this.radioService.radio;
      this.configs = this.radioService.getConfigs();
      this.radioService.getRadio
        .subscribe(radio=>{
          this.configs = this.radioService.getConfigs();
          this.radio = radio;
        });
    }
  }
  
  ngOnInit() {
  }

}
