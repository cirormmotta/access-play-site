import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../services/blog.service';
import { RadioService } from '../../../services/radio.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  radio:any;
  categories:any;
  constructor(
    private blogService: BlogService,
    private radioService:RadioService
  ) {
    this.radio = this.blogService.radio;
    this.categories = this.blogService.categories;
    this.loadCategories();
    this.radioService.getRadio
      .subscribe(radio=>{
        this.radio = radio;
        this.loadCategories();
      })
      
  }
  loadCategories(){
    console.log('-->this.categories<--', this.categories);
    if(this.radio && !this.categories){
      this.blogService.getCategories()
        .then(categories=>{
          this.blogService.categories = categories;
          this.categories = categories
        })
    }
  }
  ngOnInit() {
  }

}
