import { Component, OnInit } from '@angular/core';
import { RadioService } from '../services/radio.service';
import { BlogService } from '../services/blog.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  imageUrlArray:any;
  radio:any;
  posts:any;
  blog:any;
  categorie:any;
  offset:number = 0;
  limit:number = 10;
  pages:any;
  currentPage:string = '1';
  constructor(
    private activatedRoute: ActivatedRoute,
    private radioService : RadioService,
    private blogService : BlogService
  ) {
    this.radio = this.radioService.radio;
    this.posts = this.blogService.posts;
    this.loadPosts();
  }
  
  ngOnInit() {
    this.radioService.getRadio
      .subscribe(radio=>{
        console.log(radio);
        this.radio = radio;
        this.loadPosts();
      });
  }
  loadPosts(offset = 0, currentPage = '1'){
    if(this.radio){
      this.activatedRoute.params.subscribe(params=>{
        this.categorie = params.categorie;
        this.currentPage = currentPage;
        if(this.categorie == undefined){
          this.categorie = '';
        }
        if(offset>=0){
          this.offset = offset;
        }
        this.blogService.getPosts(this.limit,this.offset, this.categorie)
          .then(blog=>{
            this.posts = blog;
            this.blog = blog;
            this.blogService.setPosts(blog);
            this.pages = [];
            for (let index = 0; index < blog.qtd / this.limit; index++) {
              this.pages.push({
                name: (index +1).toString(),
                offset: index * this.limit,
                limit: this.limit
              });
            }
            console.log(this.pages);
            console.log(blog);
          })
      });
    }
  }

}
