/*
  documentações dos plugins
  https://angular-slider.github.io/ng5-slider/docs
  ng-simple-slideshow: https://github.com/dockleryxk/ng-simple-slideshow
*/

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SlideshowModule } from 'ng-simple-slideshow';
import { ListComponent } from './blog/components/list/list.component';
import { Ng5SliderModule } from 'ng5-slider';
import { BlogComponent } from './blog/blog.component';
import { PostComponent } from './blog/post/post.component';
import { CategoriesComponent } from './blog/components/categories/categories.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { PlayerComponent } from './components/player/player.component';
import { ShareModule } from '@ngx-share/core';
import { ShareComponent } from './components/share/share.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    BlogComponent,
    PostComponent,
    CategoriesComponent,
    HomeComponent,
    HeaderComponent,
    PlayerComponent,
    ShareComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    SlideshowModule,
    Ng5SliderModule,
    HttpClientModule,
    ShareModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
